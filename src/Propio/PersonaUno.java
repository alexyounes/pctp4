package Propio;

/**
 *
 * @author Alejandro Younes
 */
public class PersonaUno implements Runnable {

    @Override
    public void run() {
        System.out.println("1 comienza a funcionar");
        //Compartido.pedirVariasVecesLock();
        //Compartido.liberarVariasVecesLock();
        //Compartido.pedirVariasVecesTryLock();
        Compartido.liberarVariasVecesSem();
        //Compartido.pedirVariasVecesSem();
        System.out.println("1 termina");
    }
}
