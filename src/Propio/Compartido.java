package Propio;

import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author Alejandro Younes
 */
public class Compartido {

    private static ReentrantLock uno = new ReentrantLock();
    private static ReentrantLock dos = new ReentrantLock();
    private static Semaphore semUno = new Semaphore(-5, true);

    public static void pedirVariasVecesLock() {
        System.out.println(nombre() + " Pide lock 1");
        uno.lock();
        System.out.println(nombre() + " vuelve a pedir lock 1");
        uno.lock();
        System.out.println(nombre() + " nuevamente vuelve a pedir lock 1");
        uno.lock();
        System.out.println(nombre() + " no se bloqueo, lock 1 es reentrante");
        System.out.println(nombre() + " libera el lock 1 (1)");
        uno.unlock();
        System.out.println(nombre() + " libera el lock 1 (2)");
        uno.unlock();
        System.out.println(nombre() + " libera el lock 1 (3)");
        uno.unlock();
    }

    public static void pedirVariasVecesTryLock() {
        System.out.println("METODO TRYLOCK");
        System.out.println(nombre() + " Pide lock 1");
        if (uno.tryLock()) {
            System.out.println(nombre() + " vuelve a pedir lock 1");
            uno.tryLock();
            System.out.println(nombre() + " nuevamente vuelve a pedir lock 1");
            uno.tryLock();
            System.out.println(nombre() + " no se bloqueo, lock 1 es reentrante");
            System.out.println(nombre() + " libera el lock 1 (1)");
            uno.unlock();
            System.out.println(nombre() + " libera el lock 1 (2)");
            uno.unlock();
            System.out.println(nombre() + " libera el lock 1 (3)");
            uno.unlock();
        } else {
            System.out.println(nombre() + ": Lock ME BLOQUEO");
        }
    }

    public static void liberarVariasVecesLock() {
        mostrarPermisos();
        System.out.println(nombre() + " libera el lock 1 (1)");
        uno.unlock();
        mostrarPermisos();
        System.out.println(nombre() + " libera el lock 1 (2)");
        uno.unlock();
        mostrarPermisos();
        System.out.println(nombre() + " libera el lock 1 (3)");
        uno.unlock();
    }

    public static void liberarVariasVecesSem() {
        mostrarPermisosSem();
        System.out.println(nombre() + " libera el sem 1 (1)");
        semUno.release();
        mostrarPermisosSem();
        System.out.println(nombre() + " libera el sem 1 (2)");
        semUno.release();
        mostrarPermisosSem();
        System.out.println(nombre() + " libera el sem 1 (3)");
        semUno.release();
    }

    private static void mostrarPermisos() {
        System.out.println(nombre() + ": permisos en lock 1: " + uno.getHoldCount());
    }

    private static void mostrarPermisosSem() {
        System.out.println(nombre() + ": permisos disponibles en sem 1: " + semUno.availablePermits());
        //System.out.println(nombre() + ": permisos consumibles en sem 1: " + semUno.drainPermits());
        System.out.println(nombre() + ": longitud cola en sem 1: " + semUno.getQueueLength());
    }

    public static void pedirVariasVecesSem() {
        try {
            mostrarPermisosSem();
            System.out.println(nombre() + " adquiere sem 1 (1)");
            semUno.acquire();
            mostrarPermisosSem();
            System.out.println(nombre() + " adquiere sem 1 (2)");
            semUno.acquire();
            mostrarPermisosSem();
            System.out.println(nombre() + " adquiere sem 1 (3)");
            semUno.acquire();
        } catch (InterruptedException ex) {
        }
    }

    private static String nombre() {
        return Thread.currentThread().getName();
    }
}
