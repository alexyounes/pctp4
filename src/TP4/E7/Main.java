package TP4.E7;

public class Main {

    public static void main(String[] args) {
        final int NUSUARIOSA = 10;
        final int NUSUARIOSB = 10;
        final int NUSUARIOSX = 10;
        int i;
        TipoA tipoA = new TipoA(5);
        TipoB tipoB = new TipoB(5);
        Usuario usuario = new Usuario(tipoA,tipoB);
        UsuarioA usuarioA = new UsuarioA(tipoA);
        UsuarioB usuarioB = new UsuarioB(tipoB);
        Thread[] hilosA = new Thread[NUSUARIOSA];
        Thread[] hilosB = new Thread[NUSUARIOSB];
        for (i = 0; i < NUSUARIOSA; i++) {
            hilosA[i] = new Thread(usuario, String.valueOf(i));
            hilosA[i].start();
        }
        for (i = 0; i < NUSUARIOSB; i++) {
            hilosB[i] = new Thread(usuario, String.valueOf(i));
            hilosB[i].start();
        }
    }
}
