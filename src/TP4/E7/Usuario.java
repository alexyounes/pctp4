package TP4.E7;

import java.util.Random;

/**
 *
 * @author Alejandro Younes
 */
public class Usuario implements Runnable {

    TipoA tipoA;
    TipoB tipoB;
    private Impresora tipoActual;
    private Random aleatorio = new Random();

    public Usuario(TipoA tipoA) {
        this.tipoA = tipoA;
    }

    public Usuario(TipoB tipoB) {
        this.tipoB = tipoB;
    }

    public Usuario(TipoA tipoA, TipoB tipoB) {
        this.tipoA = tipoA;
        this.tipoB = tipoB;
    }

    void elegirTipo() {
        int numero = aleatorio.nextInt(2);
        if (numero == 0) {
            tipoActual = tipoA;
            System.out.println("Tipo A elegido");
        } else {
            tipoActual = tipoB;
            System.out.println("Tipo B elegido");
        }
    }

    void codigo() {
        String nombre = Thread.currentThread().getName();
        elegirTipo();
        System.out.println("Usuario " + nombre + " sin tipo pide imprimir");
        if (tipoActual.intentarAdquirir()) {
            System.out.println("Usuario " + nombre + " sin tipo imprime");
        } else {
            System.out.println("Usuario " + nombre + " sin tipo no pudo imprimir, espera");
            tipoActual.adquirir();
            System.out.println("Usuario " + nombre + " sin tipo imprime");
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            System.out.println("Excepcion en el sleep");
        }
        tipoActual.liberar();
        System.out.println("Usuario " + nombre + " sin tipo termina de imprimir");
    }

    @Override
    public void run() {
        this.codigo();
    }

}
