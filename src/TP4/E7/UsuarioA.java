package TP4.E7;

/**
 *
 * @author Alejandro Younes
 */
public class UsuarioA extends Usuario {

    public UsuarioA(TipoA tipoA) {
        super(tipoA);
    }

    @Override
    void codigo() {
        String nombre = Thread.currentThread().getName();
        System.out.println("Usuario " + nombre + " de tipo A pide imprimir");
        if (tipoA.intentarAdquirir()) {
            System.out.println("Usuario " + nombre + " de tipo A imprime");
        } else {
            System.out.println("Usuario " + nombre + " de tipo A no pudo imprimir, espera");
            tipoA.adquirir();
            System.out.println("Usuario " + nombre + " de tipo A imprime");
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            System.out.println("Excepcion en el sleep");
        }
        tipoA.liberar();
        System.out.println("Usuario " + nombre + " de tipo A termina de imprimir");
    }
}
