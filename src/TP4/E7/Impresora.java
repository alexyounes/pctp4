package TP4.E7;

import java.util.concurrent.Semaphore;

/**
 *
 * @author Alejandro Younes
 */
public abstract class Impresora {

    private final Semaphore numero;

    public Impresora(int numero) {
        this.numero = new Semaphore(numero);
    }

    public void adquirir() {
        try {
            numero.acquire();
        } catch (InterruptedException ex) {
            System.out.println("Excepcion al adquirir semaforo");
        }
    }

    public void liberar() {
        numero.release();
    }

    public boolean intentarAdquirir() {
        return numero.tryAcquire();
    }
}
