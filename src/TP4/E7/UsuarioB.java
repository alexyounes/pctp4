package TP4.E7;

/**
 *
 * @author Alejandro Younes
 */
public class UsuarioB extends Usuario {

    public UsuarioB(TipoB tipoB) {
        super(tipoB);
    }
    
    @Override
    void codigo() {
        String nombre = Thread.currentThread().getName();
        System.out.println("Usuario " + nombre + " de tipo B pide imprimir");
        if (tipoB.intentarAdquirir()) {
            System.out.println("Usuario " + nombre + " de tipo B imprime");
        } else {
            System.out.println("Usuario " + nombre + " de tipo B no pudo imprimir, espera");
            tipoB.adquirir();
            System.out.println("Usuario " + nombre + " de tipo B imprime");
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            System.out.println("Excepcion en el sleep");
        }
        tipoB.liberar();
        System.out.println("Usuario " + nombre + " de tipo B termina de imprimir");
    }
}
