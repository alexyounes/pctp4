package TP4.E5;

/**
 *
 * @author Alejandro Younes
 */
public abstract class Proceso implements Runnable {

    abstract void codigo();

    @Override
    public void run() {
        for (int i = 0; i < 3; i++) {
            this.codigo();
        }
    }

}
