package TP4.E5;

/**
 *
 * @author Alejandro Younes
 */
public class Main {

    public static void main(String[] args) {
        Proceso[] procesos = new Proceso[3];
        procesos[0] = new P1();
        procesos[1] = new P2();
        procesos[2] = new P3();
        Thread[] hilos = new Thread[3];
        int i = 0;
        for (Proceso proceso : procesos) {
            hilos[i] = new Thread(proceso);
            hilos[i].start();
            i = i + 1;
        }
    }
}
