package TP4.E5;

import java.util.concurrent.Semaphore;

/**
 *
 * @author Alejandro Younes
 */
public class Semaforos {

    private static Semaphore sem1 = new Semaphore(1);
    private static Semaphore sem2 = new Semaphore(0);
    private static Semaphore sem3 = new Semaphore(0);

    public static void adquirir(String nombre) throws InterruptedException {
        switch (nombre) {
            case "sem1":
                sem1.acquire();
                break;
            case "sem2":
                sem2.acquire();
                break;
            case "sem3":
                sem3.acquire();
                break;
        }
    }

    public static void liberar(String nombre) {
        switch (nombre) {
            case "sem1":
                sem1.release();
                break;
            case "sem2":
                sem2.release();
                break;
            case "sem3":
                sem3.release();
                break;
        }
    }
}