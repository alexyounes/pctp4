package TP4.E5;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Alejandro Younes
 */
public class P1 extends Proceso {

    @Override
    void codigo() {
        try {
            Semaforos.adquirir("sem1");
        } catch (InterruptedException ex) {
            Logger.getLogger(P1.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Proceso P1");
        Semaforos.liberar("sem3");
    }
}
