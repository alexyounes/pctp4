package TP4.E5;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Alejandro Younes
 */
public class P3 extends Proceso {

    @Override
    void codigo() {
        try {
            Semaforos.adquirir("sem3");
        } catch (InterruptedException ex) {
            Logger.getLogger(P1.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Proceso P3");
        Semaforos.liberar("sem2");
    }
}
