package TP4.E14_v2;

import static TP4.E14.UtilidadesHilos.nombre;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author Alejandro Younes
 */
public class Confiteria {

    private static Semaphore semMozo = new Semaphore(0);
    private static Semaphore[] semSilla = {new Semaphore(1), new Semaphore(1)};
    private static Semaphore[] semCocinero = {new Semaphore(0), new Semaphore(0)};
    private static Semaphore[] semEmpleado = {new Semaphore(0), new Semaphore(0)};
    private static int sillaBebida;
    private static int[] sillaComida = {-1, -1};
    private static ReentrantLock cerrojoMozo = new ReentrantLock();
    private static ReentrantLock[] cerrojoCocinero = {new ReentrantLock(), new ReentrantLock()};

    public static int ocuparSilla() {// Metodo que usa el empleado para ocupar una silla
        int numSilla = -1;// Numero que se retorna si no se consigue sentar
        int i = 0;
        int n = semSilla.length;
        boolean sentado = false;
        while (!sentado && i < n) {// Mientras no este sentado y hayan sillas sin consultar
            sentado = semSilla[i].tryAcquire();// Trata de sentarse en la silla i
            if (sentado) {// Si se pudo sentar
                numSilla = i;// Memoriza el numero de silla
            }
            i = i + 1;
        }
        return numSilla;
    }

    public static void desocuparSilla(int numSilla) {// Metodo que usa el empleado para desocupar su silla
        System.out.println("Emplead@ " + nombre() + ": termino de comer, desocupa la silla " + numSilla + " y agradece al mozo");
        semSilla[numSilla].release();//Emplead@ libera silla numSilla
    }

    public static void inventarPollos() {// Metodo que usa el Mozo para esperar empleados
        try {
            semMozo.acquire();//Mozo se bloquea
        } catch (InterruptedException ex) {
            System.out.println("Excepcion al adquirir semaforo mozo");
        }
        System.out.println(nombre() + ": No hay nadie a quien atender, invento versiones de pollo");
    }

    public static void ordenarCocina(int numCocinero) {// Metodo que usa el Cocinero para esperar empleados
        sillaComida[numCocinero] = -1;
        try {
            semCocinero[numCocinero].acquire();//Cocinero numCocinero se bloquea
        } catch (InterruptedException ex) {
            System.out.println("Excepcion al adquirir semaforo cocinero");
        }
        System.out.println(nombre() + ": No hay nada que cocinar, ordeno cocina");
    }

    public static void avisarMozo(int numSilla) {// Metodo que usa el Emplead@ para pedir bebida
        System.out.println("Emplead@ " + nombre() + ": pide al mozo la bebida");
        sillaBebida = numSilla;//Emplead@ almacena su silla en sillaBebida
        semMozo.release();//Emplead@ indica al mozo que puede buscar bebida
    }

    public static int tomarNumPedidoBebida() {// Metodo que usa el mozo para saber silla donde lleva bebida
        return sillaBebida;
    }

    public static int tomarNumPedidoComida(int numCocinero) {// Metodo que usa el cocinero para saber silla donde lleva comida
        return sillaComida[numCocinero];
    }

    public static void avisarCocinero(int numSilla) {// Metodo que usa el empleado para pedir comida
        boolean disponible = false;
        int i = 0;
        int numCocinero = -1;
        while (!disponible) {
            System.out.println(i);
            cerrojoCocinero[i].lock();
            if (sillaComida[i] == -1) {
                sillaComida[i] = numSilla;
                cerrojoCocinero[i].unlock();
                disponible = true;
                numCocinero = i;
            } else {
                cerrojoCocinero[i].unlock();
                i = (i + 1) % 2;
            }
        }
        System.out.println("Emplead@ " + nombre() + ": pide la comida al cocinero " + numCocinero);
        semCocinero[numCocinero].release();//Emplead@ indica al cocinero numCocinero que puede cocinar
    }

    public static void esperar(int numSilla, String cosa) {// Metodo que usa el empleado para esperar bebida o comida
        System.out.println("Emplead@ " + nombre() + ": espera pacientemente sentad@ en la silla " + numSilla + " que le sirvan la " + cosa);
        try {
            semEmpleado[numSilla].acquire();//emplead@ en numSilla se bloquea
        } catch (InterruptedException ex) {
            System.out.println("Excepcion al adquirir semaforo empleado");
        }
    }

    public static void servir(int numSilla, String cosa) {// Metodo que usa el mozo/cocinero para servir bebida/comida
        System.out.println(nombre() + ": sirve " + cosa + " al emplead@ en la silla " + numSilla);
        semEmpleado[numSilla].release();//Indica al emplead@ en numSilla que puede seguir
    }
}
