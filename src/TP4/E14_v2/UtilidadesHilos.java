package TP4.E14_v2;

/**
 *
 * @author Alejandro Younes
 */
public class UtilidadesHilos {

    public static String nombre() {
        return Thread.currentThread().getName();
    }

    public static void simularActividad(String actividad, int milisegundos) {
        System.out.println(nombre() + ": empieza a " + actividad); //Mensaje de inicio
        try {
            Thread.sleep(milisegundos);//Tiempo de simulacion
        } catch (InterruptedException ex) {
            System.out.println("Excepcion en el sleep");
        }
        System.out.println(nombre() + ": termina de " + actividad);// Mensaje de fin
    }
}
