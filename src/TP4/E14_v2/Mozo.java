package TP4.E14_v2;

import static TP4.E14.UtilidadesHilos.nombre;

/**
 *
 * @author Alejandro Younes
 */
public class Mozo implements Runnable {

    @Override
    public void run() {
        while (true) {// Supongo que el mozo siempre esta en la confiteria
            Confiteria.inventarPollos();// No hay nadie a quien servir, espera
            int numSilla = Confiteria.tomarNumPedidoBebida();// Toma numero de la silla del emplead@ que lo libero
            System.out.println(nombre() + ": recibe pedido del cliente en silla " + numSilla + " y busca bebida");
            Confiteria.servir(numSilla, "bebida");// Sirve bebida al emplead@ que lo libero
        }
    }
}
