package TP4.E3;

/**
 *
 * @author Alejandro Younes
 */
public class Restador implements Runnable {

    SynchronizedObjectCounterC contador;

    public Restador(SynchronizedObjectCounterC contador) {
        this.contador = contador;
    }

    @Override
    public void run() {
        for (int i = 1; i <= 10000; i++) {
            contador.decrement();
        }
    }

}
