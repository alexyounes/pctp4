package TP4.E3;

/**
 *
 * @author Alejandro Younes
 */
public class SynchronizedObjectCounterC {

    private int c = 0;

    public void increment() {
        synchronized (this) { // Este elemento debe ser casteado a Integer
            c++;
        }
    }

    public void decrement() {
        synchronized (this) {
            c--;
        }
    }

    public int value() {
        return c;
    }
}
