package TP4.E3;

/**
 *
 * @author Alejandro Younes
 */
public class Sumador implements Runnable {

    SynchronizedObjectCounterC contador;

    public Sumador(SynchronizedObjectCounterC contador) {
        this.contador = contador;
    }

    @Override
    public void run() {
        for (int i = 1; i <= 10000; i++) {
            contador.increment();
        }
    }

}
