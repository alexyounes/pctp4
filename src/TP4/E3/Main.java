package TP4.E3;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Alejandro Younes
 */
public class Main {

    public static void main(String[] args) {
        SynchronizedObjectCounterC contador = new SynchronizedObjectCounterC();
        Restador restador = new Restador(contador);
        Sumador sumador = new Sumador(contador);
        Thread hiloRes1 = new Thread(restador);
        Thread hiloSum1 = new Thread(sumador);
        hiloRes1.start();
        hiloSum1.start();
        try {
            hiloRes1.join();
            hiloSum1.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(contador.value());
    }
}
