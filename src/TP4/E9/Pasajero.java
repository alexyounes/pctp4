package TP4.E9;

/**
 *
 * @author Alejandro Younes
 */
public class Pasajero implements Runnable {

    private Taxi taxi;

    public Pasajero(Taxi taxi) {
        this.taxi = taxi;
    }

    @Override
    public void run() {
        if (taxi.tomarTaxi()) {
            System.out.println("Pasajero " + Thread.currentThread().getName() + " toma taxi");
            taxi.despertarTaxista();
            taxi.esperarLlegada();
            taxi.bajarseTaxi();
        } else {
            System.out.println("Pasajero " + Thread.currentThread().getName() + " no pudo tomar taxi");
        }
    }
}
