package TP4.E9;

import java.util.concurrent.Semaphore;

/**
 *
 * @author Alejandro Younes
 */
public class Taxi {

    private Semaphore taxista = new Semaphore(0);
    private Semaphore pasajero = new Semaphore(0);
    private Semaphore asiento = new Semaphore(1);

    public void esperarPasajero() {
        System.out.println("Taxista duerme en el taxi");
        try {
            taxista.acquire();
        } catch (InterruptedException ex) {
            System.out.println("Error al adquirir semaforo del taxista");
        }
        System.out.println("Taxista despierta");
    }

    public boolean tomarTaxi() {
        return asiento.tryAcquire();
    }

    public void despertarTaxista() {
        System.out.println("Pasajero " + Thread.currentThread().getName() + " despierta al taxista");
        taxista.release();
    }

    public void esperarLlegada() {
        System.out.println("Pasajero " + Thread.currentThread().getName() + " espera llegar a destino");
        try {
            pasajero.acquire();
        } catch (InterruptedException ex) {
            System.out.println("Error al adquirir semaforo del pasajero");
        }
    }

    public void avisarPasajero() {
        System.out.println("Taxista avisa al pasajero que llego a destino");
        pasajero.release();
    }

    public void bajarseTaxi() {
        System.out.println("Pasajero " + Thread.currentThread().getName() + " paga al taxista y baja del taxi");
        asiento.release();
    }
}
