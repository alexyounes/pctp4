package TP4.E9;

/**
 *
 * @author Alejandro Younes
 */
public class Taxista implements Runnable {

    private Taxi taxi;

    public Taxista(Taxi taxi) {
        this.taxi = taxi;
    }

    @Override
    public void run() {
        taxi.esperarPasajero();
        System.out.println("Taxista realiza viaje al destino del pasajero");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            System.out.println("Error en sleep del taxista");
        }
        taxi.avisarPasajero();
    }

}
