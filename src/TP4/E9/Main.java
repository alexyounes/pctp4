package TP4.E9;

/**
 *
 * @author Alejandro Younes
 */
public class Main {

    public static void main(String[] args) {
        String nombres[] = {"Pepita", "Pepito", "Juancita", "Juancito"};
        Taxi taxi = new Taxi();
        Pasajero pasajero = new Pasajero(taxi);
        Taxista taxista = new Taxista(taxi);
        Thread[] hilosP = new Thread[4];
        Thread hiloT = new Thread(taxista);
        hiloT.start();
        for (int i = 0; i < 4; i++) {
            hilosP[i] = new Thread(pasajero, nombres[i]);
            hilosP[i].start();
        }
    }
}

/*
Mientras usted esta ocupado caminando por la calle buscando
un taxi, el taxista está durmiendo plácidamente en la cabina. Cuando usted le avisa que quiere
tomar el taxi, él se despierta y comienza a conducir. Cuando usted arriba a su destino, el taxista
le notifica que ha llegado y continúa con su trabajo. El taxista debe ahora esperar y dormir
nuevamente una siesta hasta que el próximo pasajero llegue.
 */
