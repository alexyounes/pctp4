package TP4.E14;

import static TP4.E14.UtilidadesHilos.nombre;
import static TP4.E14.UtilidadesHilos.simularActividad;

/**
 *
 * @author Alejandro Younes
 */
public class Cocinero implements Runnable {

    private int numCocinero;

    public Cocinero(int numCocinero) {
        this.numCocinero = numCocinero;
    }

    @Override
    public void run() {
        while (true) {// Supongo que el cocinero siempre esta en la confiteria
            Confiteria.ordenarCocina(numCocinero);// No hay nada que cocinar, espera
            int numSilla = Confiteria.tomarNumPedidoComida(numCocinero);// Toma numero de la silla del emplead@ que lo libero
            System.out.println(nombre() + ": recibe pedido del cliente en silla " + numSilla);
            simularActividad("cocinar", 1000);// Cocina por 1 segundo
            Confiteria.servir(numSilla, "comida");// Sirve comida
        }
    }
}
