package TP4.E14;

/**
 *
 * @author Alejandro Younes
 */
public class Main {

    public static void main(String[] args) {
        Empleado[] empleados = new Empleado[6];
        Thread[] empleadosT = new Thread[6];
        String[] nombres = {"Pepita", "Pepito", "Juancita", "Juancito", "Luisita", "Luisito"};
        Mozo mozo = new Mozo();
        Thread hiloMozo = new Thread(mozo, "Mozo");
        Cocinero cocinero0 = new Cocinero(0);
        Cocinero cocinero1 = new Cocinero(1);
        Thread hiloCocinero0 = new Thread(cocinero0, "Cocinero0");
        Thread hiloCocinero1 = new Thread(cocinero1, "Cocinero1");
        hiloMozo.start();
        hiloCocinero0.start();
        hiloCocinero1.start();
        for (int i = 0; i < 6; i++) {
            empleados[i] = new Empleado();
            empleadosT[i] = new Thread(empleados[i], nombres[i]);
            empleadosT[i].start();
        }
    }
}
