package TP4.E14;

import static TP4.E14.UtilidadesHilos.nombre;
import static TP4.E14.UtilidadesHilos.simularActividad;

/**
 *
 * @author Alejandro Younes
 */
public class Empleado implements Runnable {

    private int numSilla;

    @Override
    public void run() {
        while (true) {
            System.out.println("Emplead@ " + nombre() + " tiene hambre, va a la confiteria");
            numSilla = Confiteria.ocuparSilla();// Intenta ocupar silla
            if (numSilla != -1) {// Si pudo ocupar una silla
                System.out.println("Emplead@ " + nombre() + " encuentra la silla " + numSilla + " desocupada, se sienta");
                int opcion = (int) (Math.random() * 3);// Obtiene opcion aleatoria entre 0 y 2
                switch (opcion) {
                    case 0:// Solo bebida
                        System.out.println("Emplead@ " + nombre() + " solo quiere beber algo");
                        pedirBebida();
                        break;
                    case 1:// Solo comida
                        System.out.println("Emplead@ " + nombre() + " solo quiere comer algo");
                        pedirComida();
                        break;
                    case 2:// Bebida y Comida
                        System.out.println("Emplead@ " + nombre() + " quiere beber y comer");
                        pedirBebida();
                        pedirComida();
                        break;
                }
                simularActividad("comer", 1000);// Come por 1 segundo
                Confiteria.desocuparSilla();// Desocupa la silla y se va
            } else {// Si no pudo ocupar una silla
                System.out.println("Emplead@ " + nombre() + " encuentra todas las sillas ocupadas");
            }
            System.out.println("Emplead@ " + nombre() + " vuelve a trabajar");
            simularActividad("trabajar", 5000);// Trabaja por 5 segundos
        }
    }

    private void pedirBebida() {
        Confiteria.avisarMozo(numSilla);
        Confiteria.esperar(numSilla, "bebida");
        Confiteria.recibirBebida();
    }

    private void pedirComida() {
        int numCocinero = Confiteria.avisarCocinero(numSilla);
        Confiteria.esperar(numSilla, "comida");
        Confiteria.recibirComida(numCocinero);
    }
}
