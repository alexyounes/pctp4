package TP4.E14;

import static TP4.E14.UtilidadesHilos.nombre;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author Alejandro Younes
 */
public class Confiteria {

    private static Semaphore semMozo = new Semaphore(0, true);
    private static Semaphore[] semCocinero = {new Semaphore(0, true), new Semaphore(0, true)};
    private static Semaphore[] semEmpleado = {new Semaphore(0, true), new Semaphore(0, true)};
    private static ReentrantLock lockBebida = new ReentrantLock(); //Para evitar que el mozo atienda a más de uno por vez
    private static ReentrantLock[] lockComida = {new ReentrantLock(), new ReentrantLock()}; //Para evitar que cada cocinero atienda a más de uno por vez
    private static ReentrantLock lockSillas = new ReentrantLock(); //Para mantener consistencia de sillasDisponibles
    private static int sillaBebida; //Para almacenar silla que esta atendiendo el mozo
    private static int[] sillaComida = new int[2]; //Para almacenar silla que esta atendiendo cada cocinero
    private static int sillasDisponibles = 2;

    public static int ocuparSilla() {// Metodo que usa emplead@ para ocupar una silla
        int silla = -1; // Valor inicial, en caso de que no haya sillas libres
        lockSillas.lock(); // Evita que otr@ emplead@ altere la cantidad de sillas libres
        if (sillasDisponibles > 0) { // Si hay alguna silla libre
            sillasDisponibles = sillasDisponibles - 1; // Reduce en uno la cantidad de sillas libres
            silla = sillasDisponibles; // Indica que silla ocupo
        }
        lockSillas.unlock(); // Permite que otr@ emplead@ altere la cantidad de sillas libres
        return silla;
    }

    public static void desocuparSilla() {// Metodo que usa emplead@ para desocupar su silla
        System.out.println("Emplead@ " + nombre() + ": termino de comer, desocupa la silla y agradece al mozo");
        lockSillas.lock(); // Evita que otr@ emplead@ altere la cantidad de sillas libres
        sillasDisponibles = sillasDisponibles + 1; // Aumenta en uno la cantidad de sillas libres
        lockSillas.unlock(); // Permite que otr@ emplead@ altere la cantidad de sillas libres
    }

    public static void inventarPollos() {// Metodo que usa el Mozo para esperar emplead@s
        System.out.println(nombre() + ": No hay nadie a quien atender, invento versiones de pollo");
        try {
            semMozo.acquire();//Mozo se bloquea
        } catch (InterruptedException ex) {
            System.out.println("Excepcion al adquirir semaforo mozo");
        }
    }

    public static void ordenarCocina(int numCocinero) {// Metodo que usa el Cocinero para esperar empleados
        System.out.println(nombre() + ": No hay nada que cocinar, ordeno cocina");
        try {
            semCocinero[numCocinero].acquire();//Cocinero numCocinero se bloquea
        } catch (InterruptedException ex) {
            System.out.println("Excepcion al adquirir semaforo cocinero");
        }
    }

    public static void avisarMozo(int numSilla) {// Metodo que usa el Emplead@ para pedir bebida
        lockBebida.lock();// Emplead@ impide que otr@ emplead@ pida bebida al mozo
        System.out.println("Emplead@ " + nombre() + ": pide al mozo la bebida");
        sillaBebida = numSilla;//Emplead@ indica su silla al mozo
        semMozo.release();//Emplead@ indica al mozo que puede buscar bebida
    }

    public static int tomarNumPedidoBebida() {// Metodo que usa el mozo para saber silla donde lleva bebida
        return sillaBebida;
    }

    public static void recibirBebida() {// Metodo que usa emplead@ para indicar que recibio bebida
        lockBebida.unlock(); // Emplead@ permite que otr@ emplead@ pida bebida al mozo
    }

    public static int tomarNumPedidoComida(int numCocinero) {// Metodo que usa el cocinero para saber silla donde lleva comida
        return sillaComida[numCocinero];
    }

    public static void recibirComida(int numCocinero) {// Metodo que usa emplead@ para indicar que recibio comida
        lockComida[numCocinero].unlock(); // Emplead@ permite que otr@ emplead@ pida comida al cocinero numCocinero
    }

    public static int avisarCocinero(int numSilla) {// Metodo que usa el empleado para pedir comida
        int numCocinero;
        if (lockComida[0].tryLock()) {// Emplead@ trata de impedir que otr@ emplead@ pida comida al cocinero 0
            numCocinero = 0;// Emplead@ encuentra libre al cocinero 0
        } else {
            lockComida[1].lock();// Emplead@ impide que otr@ emplead@ pida comida al cocinero 1
            numCocinero = 1;// Emplead@ encuentra libre al cocinero 1
        }
        System.out.println("Emplead@ " + nombre() + " de la silla " + numSilla + " pide comida al cocinero " + numCocinero);
        sillaComida[numCocinero] = numSilla;//Emplead@ indica su silla al cocinero numCocinero
        semCocinero[numCocinero].release();//Emplead@ indica al cocinero numCocinero que puede cocinar
        return numCocinero;
    }

    public static void esperar(int numSilla, String cosa) {// Metodo que usa el empleado para esperar bebida o comida
        System.out.println("Emplead@ " + nombre() + ": espera pacientemente sentad@ en la silla " + numSilla + " que le sirvan la " + cosa);
        try {
            semEmpleado[numSilla].acquire();//emplead@ en numSilla se bloquea
        } catch (InterruptedException ex) {
            System.out.println("Excepcion al adquirir semaforo empleado");
        }
    }

    public static void servir(int numSilla, String cosa) {// Metodo que usa el mozo/cocinero para servir bebida/comida
        System.out.println(nombre() + ": sirve " + cosa + " al emplead@ en la silla " + numSilla);
        semEmpleado[numSilla].release();//Indica al emplead@ en numSilla que puede seguir
    }
}
