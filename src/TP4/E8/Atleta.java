package TP4.E8;

import java.util.Random;

/**
 *
 * @author Alejandro Younes
 */
public class Atleta implements Runnable {

    public void corre() {
        long inicio, fin;
        Random aleatorio = new Random();
        int tiempo = (aleatorio.nextInt(3) + 9) * 1000;
        String nombre = Thread.currentThread().getName();
        System.out.println("Atleta " + nombre + " comienza su carrera");
        try {
            inicio = System.currentTimeMillis();
            Thread.sleep(tiempo);
            fin = System.currentTimeMillis() - inicio;
            System.out.println("Atleta " + nombre + " finalizo en " + fin / 1000 + "s");
        } catch (InterruptedException ex) {
            System.out.println("Excepcion en el sleep del atleta " + nombre);
        }
    }

    @Override
    public void run() {
        Testigo.tomarTestigo();
        this.corre();
        Testigo.soltarTestigo();
    }
}
