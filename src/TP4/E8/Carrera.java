package TP4.E8;

/**
 *
 * @author Alejandro Younes
 */
public class Carrera {

    public static void main(String[] args) {
        Atleta atleta = new Atleta();
        Thread[] hilos = new Thread[4];
        for (int i = 0; i < 4; i++) {
            hilos[i] = new Thread(atleta, String.valueOf(i));
            hilos[i].start();
        }
    }
}
