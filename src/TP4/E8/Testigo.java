package TP4.E8;

import java.util.concurrent.Semaphore;

/**
 *
 * @author Alejandro Younes
 */
public class Testigo {

    public static Semaphore testigo = new Semaphore(1);

    public static void tomarTestigo() {
        try {
            testigo.acquire();
        } catch (InterruptedException ex) {
            System.out.println("Excepcion al tomar el testigo");
        }
    }

    public static void soltarTestigo() {
        testigo.release();
    }
}
