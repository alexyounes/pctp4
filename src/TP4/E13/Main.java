package TP4.E13;

/**
 *
 * @author Alejandro Younes
 */
public class Main {

    public static void main(String[] args) {
        Mozo mozo = new Mozo();
        Cocinero cocinero = new Cocinero();
        Empleado empleado = new Empleado();
        Thread[] empleados = new Thread[6];
        String[] nombres = {"Pepita", "Pepito", "Juancita", "Juancito", "Luisita", "Luisito"};
        Thread hiloMozo = new Thread(mozo);
        Thread hiloCocinero = new Thread(cocinero);
        hiloMozo.start();
        hiloCocinero.start();
        for (int i = 0; i < 6; i++) {
            empleados[i] = new Thread(empleado, nombres[i]);
            empleados[i].start();
        }
    }
}
