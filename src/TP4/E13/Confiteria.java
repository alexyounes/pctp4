package TP4.E13;

import java.util.concurrent.Semaphore;

/**
 *
 * @author Alejandro Younes
 */
public class Confiteria {

    static private Semaphore silla = new Semaphore(1);
    static private Semaphore mozo = new Semaphore(0);
    static private Semaphore cocinero = new Semaphore(0);
    static private Semaphore empleado = new Semaphore(0);

    public static boolean ocuparSilla() {
        return silla.tryAcquire();
    }

    public static void desocuparSilla() {
        System.out.println("Empleado " + Thread.currentThread().getName() + " termino de comer, desocupa la silla y agradece al mozo");
        silla.release();
    }

    public static void inventarPollos() {
        try {
            mozo.acquire();
        } catch (InterruptedException ex) {
            System.out.println("Excepcion al adquirir semaforo mozo");
        }
        System.out.println("Mozo: No hay nadie a quien atender, invento versiones de pollo");
    }

    public static void ordenarCocina() {
        try {
            cocinero.acquire();
        } catch (InterruptedException ex) {
            System.out.println("Excepcion al adquirir semaforo cocinero");
        }
        System.out.println("Cocinero: No hay nada que cocinar, ordeno cocina");
    }

    public static void avisarMozo() {
        System.out.println("Empleado " + Thread.currentThread().getName() + " pide al mozo la lista");
        mozo.release();
    }

    public static void avisarCocinero() {
        System.out.println("Mozo: recibe pedido del cliente y lo da al cocinero");
        cocinero.release();
    }

    public static void esperarCocinero() {
        System.out.println("Mozo: espera a que el cocinero termine");
        try {
            mozo.acquire();
        } catch (InterruptedException ex) {
            System.out.println("Excepcion al adquirir semaforo mozo");
        }
    }

    public static void comidaLista() {
        System.out.println("Cocinero: avisa al mozo que termino");
        mozo.release();
    }

    public static void esperarComida() {
        System.out.println("Empleado " + Thread.currentThread().getName() + " espera pacientemente que le sirvan");
        try {
            empleado.acquire();
        } catch (InterruptedException ex) {
            System.out.println("Excepcion al adquirir semaforo empleado");
        }
    }

    public static void servirComida() {
        System.out.println("Mozo: sirve comida al empleado");
        empleado.release();
    }
}
