package TP4.E13;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Alejandro Younes
 */
public class Empleado implements Runnable {

    @Override
    public void run() {
        while (true) {
            try {
                System.out.println("Emplead@ " + Thread.currentThread().getName() + " tiene hambre, va a la confiteria");
                if (Confiteria.ocuparSilla()) {// Intenta ocupar silla
                    System.out.println("Emplead@ " + Thread.currentThread().getName() + " encuentra la silla desocupada, se sienta");
                    Confiteria.avisarMozo();// Avisa al mozo que tiene hambre
                    Confiteria.esperarComida();// Espera que le sirvan
                    System.out.println("Emplead@ " + Thread.currentThread().getName() + " empieza a comer");
                    Thread.sleep(1000);// Simula tiempo comiendo
                    Confiteria.desocuparSilla();// Desocupa la silla y se va
                } else {// Si no pudo ocupar la silla
                    System.out.println("Emplead@ " + Thread.currentThread().getName() + " encuentra la silla ocupada, vuelve a trabajar un rato");
                    Thread.sleep(1000);// Simula tiempo trabajando
                }
            } catch (InterruptedException ex) {
                System.out.println("Excepcion en el sleep");
            }
            System.out.println("Emplead@ " + Thread.currentThread().getName() + " vuelve a trabajar");
            try {
                Thread.sleep(5000);// Simula tiempo trabajando
            } catch (InterruptedException ex) {
                System.out.println("Excepcion en el sleep");
            }
        }
    }
}
