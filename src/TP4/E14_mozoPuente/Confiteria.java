package TP4.E14_mozoPuente;

import java.util.concurrent.Semaphore;

/**
 *
 * @author Alejandro Younes
 */
public class Confiteria {

    private static Semaphore semMozo = new Semaphore(0);
    private static Semaphore[] semSilla = {new Semaphore(1), new Semaphore(1)};
    private static Semaphore[] semCocinero = {new Semaphore(0), new Semaphore(0)};
    private static Semaphore[] semEmpleado = {new Semaphore(0), new Semaphore(0)};
    int sillasLibres = 2;

    public static int ocuparSilla() {
        int numSilla;
        int i = -1;
        int n = semSilla.length;
        boolean sentado = false;
        while (!sentado && i < n) {
            i = i + 1;
            sentado = semSilla[i].tryAcquire();
        }
        if (sentado) {
            numSilla = i;
        } else {
            numSilla = -1;
        }
        return numSilla;
    }

    public static void desocuparSilla(int numSilla) {
        System.out.println("Empleado " + Thread.currentThread().getName() + " termino de comer, desocupa su silla y agradece al mozo");
        semSilla[numSilla].release();
    }

    public static void inventarPollos() {
        try {
            semMozo.acquire();
        } catch (InterruptedException ex) {
            System.out.println("Excepcion al adquirir semaforo mozo");
        }
        System.out.println("Mozo: No hay nadie a quien atender, invento versiones de pollo");
    }

    public static void ordenarCocina(int numCocinero) {
        try {
            semCocinero[numCocinero].acquire();
        } catch (InterruptedException ex) {
            System.out.println("Excepcion al adquirir semaforo cocinero");
        }
        System.out.println("Cocinero: No hay nada que cocinar, ordeno cocina");
    }

    public static void avisarMozo() {
        System.out.println("Empleado " + Thread.currentThread().getName() + " pide al mozo la lista");
        semMozo.release();
    }

    public static void avisarCocinero(int numCocinero) {
        System.out.println("Mozo: recibe pedido del cliente y lo da al cocinero");
        semCocinero[numCocinero].release();
    }

    public static void esperarCocinero() {
        System.out.println("Mozo: espera a que el cocinero termine");
        try {
            semMozo.acquire();
        } catch (InterruptedException ex) {
            System.out.println("Excepcion al adquirir semaforo mozo");
        }
    }

    public static void comidaLista() {
        System.out.println("Cocinero: avisa al mozo que termino");
        semMozo.release();
    }

    public static void esperarComida(int numEmpleado) {
        System.out.println("Empleado " + Thread.currentThread().getName() + " espera pacientemente que le sirvan");
        try {
            semEmpleado[numEmpleado].acquire();
        } catch (InterruptedException ex) {
            System.out.println("Excepcion al adquirir semaforo empleado");
        }
    }

    public static void servirComida(int numEmpleado) {
        System.out.println("Mozo: sirve comida al empleado");
        semEmpleado[numEmpleado].release();
    }
}
