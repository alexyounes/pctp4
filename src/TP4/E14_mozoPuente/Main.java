package TP4.E14_mozoPuente;

/**
 *
 * @author Alejandro Younes
 */
public class Main {

    public static void main(String[] args) {
        Mozo mozo = new Mozo();
        Cocinero cocinero1 = new Cocinero(1);
        Cocinero cocinero2 = new Cocinero(2);
        Empleado empleado = new Empleado();
        Thread[] empleados = new Thread[6];
        String[] nombres = {"Pepita", "Pepito", "Juancita", "Juancito", "Luisita", "Luisito"};
        Thread hiloMozo = new Thread(mozo);
        Thread hiloCocinero1 = new Thread(cocinero1);
        Thread hiloCocinero2 = new Thread(cocinero2);
        hiloMozo.start();
        hiloCocinero1.start();
        hiloCocinero2.start();
        for (int i = 0; i < 6; i++) {
            empleados[i] = new Thread(empleado, nombres[i]);
            empleados[i].start();
        }
    }
}
