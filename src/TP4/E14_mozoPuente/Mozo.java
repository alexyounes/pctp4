package TP4.E14_mozoPuente;

/**
 *
 * @author Alejandro Younes
 */
public class Mozo implements Runnable {

    @Override
    public void run() {
        while (true) {// Supongo que el mozo siempre esta en la confiteria
            Confiteria.inventarPollos();// No hay nadie a quien servir, espera
            //Confiteria.avisarCocinero(numCocinero);// Al Recibir pedido del cliente avisa al cocinero
            Confiteria.esperarCocinero();// Espera que el cocinero termine de cocinar
            //Confiteria.servirComida(numEmpleado);//Sirve la comida al empleado
        }
    }
}
