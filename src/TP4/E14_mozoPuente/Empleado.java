package TP4.E14_mozoPuente;

/**
 *
 * @author Alejandro Younes
 */
public class Empleado implements Runnable {

    private int numSilla;

    @Override
    public void run() {
        while (true) {
            try {
                System.out.println("Emplead@ " + Thread.currentThread().getName() + " tiene hambre, va a la confiteria");
                numSilla = Confiteria.ocuparSilla(); // Intenta ocupar silla
                if (numSilla != -1) { // Si pudo ocupar una silla
                    System.out.println("Emplead@ " + Thread.currentThread().getName() + " encuentra la silla desocupada, se sienta");
                    Confiteria.avisarMozo();// Avisa al mozo que tiene hambre
                    Confiteria.esperarComida(numSilla);// Espera que le sirvan
                    System.out.println("Emplead@ " + Thread.currentThread().getName() + " empieza a comer");
                    Thread.sleep(1000);// Simula tiempo comiendo
                    Confiteria.desocuparSilla(numSilla);// Desocupa la silla y se va
                } else {// Si no pudo ocupar una silla
                    System.out.println("Emplead@ " + Thread.currentThread().getName() + " encuentra la silla ocupada, vuelve a trabajar un rato");
                    Thread.sleep(1000);// Simula tiempo trabajando
                }
            } catch (InterruptedException ex) {
                System.out.println("Excepcion en el sleep");
            }
            System.out.println("Emplead@ " + Thread.currentThread().getName() + " vuelve a trabajar");
            try {
                Thread.sleep(5000);// Simula tiempo trabajando
            } catch (InterruptedException ex) {
                System.out.println("Excepcion en el sleep");
            }
        }
    }
}
