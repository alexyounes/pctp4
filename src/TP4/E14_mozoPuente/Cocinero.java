package TP4.E14_mozoPuente;

/**
 *
 * @author Alejandro Younes
 */
public class Cocinero implements Runnable {

    private int numCocinero;

    public Cocinero(int numCocinero) {
        this.numCocinero = numCocinero;
    }

    @Override
    public void run() {
        while (true) {// Supongo que el cocinero siempre esta en la confiteria
            Confiteria.ordenarCocina(numCocinero);// No hay nada que cocinar, espera
            System.out.println("Cocinero: recibe pedido del mozo, empieza a cocinar");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                System.out.println("Excepcion en el sleep");
            }
            Confiteria.comidaLista();
        }
    }
}
