package TP4.E6;

import Utilidades.Color;
import Utilidades.Color.Colores;

public class Imprimidor implements Runnable {

    private final char letra;
    private final int SECUENCIAS = 10;
    final int LETRASDIS = 3;
    private final int veces;
    private final Turno turno;
    private final int numero;
    final Colores COLOR;

    public Imprimidor(char letra, int veces, Turno turno, int numero, Colores color) {
        this.letra = letra;
        this.veces = veces;
        this.turno = turno;
        this.numero = numero;
        this.COLOR = color;
    }

    @Override
    public void run() {
        int proximoHilo, i, j;
        for (i = 1; i <= SECUENCIAS; i++) { // Por cada secuencia a imprimir
            turno.adquirir(numero); // Adquiere su turno
            for (j = 1; j <= veces; j++) {
                Color.escribirColor(letra, COLOR); // Si es su turno imprime su letra X veces
            }
            proximoHilo = (numero % LETRASDIS) + 1;// Determina quien el proximo hilo
            turno.liberar(proximoHilo);           // Pasa el turno al proximo hilo
        }
    }
}
