package TP4.E6;

import Utilidades.Color.Colores;

public class Main {

    public static void main(String[] args) {
        final int LETRASDIS = 3;   // Cantidad de letras distintas a imprimir 
        char letra;
        int veces, numHilo;
        Colores color;
        Turno turno = new Turno(LETRASDIS);// Variable compartida para el tunro
        Imprimidor[] imprimidores = new Imprimidor[LETRASDIS];// Arreglo para los objetos impresores
        Thread[] hilos = new Thread[LETRASDIS];               // Arreglo para los hilos
        Colores[] colores = Colores.values(); // Arreglo para almacenar el color de cada letra
        for (int i = 0; i < LETRASDIS; i++) { // Para cada letra distinta
            letra = (char) (i + 65);// Convierte valor ASCII a caracter
            veces = i + 1;          // Cantidad de veces que se imprime la letra
            numHilo = i + 1;        // Numero para identificar al hilo
            color = colores[i];     // Asignar como color el que esta en la posicion i
            imprimidores[i] = new Imprimidor(letra, veces, turno, numHilo, color);
            hilos[i] = new Thread(imprimidores[i]);// Crea un nuevo hilo con el impresor
            hilos[i].start();                      // y lo pone a ejecutar
        }
    }
}
