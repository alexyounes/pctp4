package TP4.E6;

import java.util.concurrent.Semaphore;

public class Turno {

    private Semaphore[] semaforos;

    public Turno(int hilos) {
        semaforos = new Semaphore[hilos];
        semaforos[0] = new Semaphore(1);
        for (int i = 1; i < hilos; i++) {
            semaforos[i] = new Semaphore(0);
        }
    }

    public void adquirir(int i) {
        i = i - 1;
        try {
            semaforos[i].acquire();
        } catch (InterruptedException ex) {
            System.out.println("Excepcion al adquirir semaforo " + i);
        }
    }

    public void liberar(int i) {
        i = i - 1;
        semaforos[i].release();
    }
}
