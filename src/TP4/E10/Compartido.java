package TP4.E10;

import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author Alejandro Younes
 */
public class Compartido {

    ReentrantLock cerrojo = new ReentrantLock();

    public void seccionCritica() {
        System.out.println(Thread.currentThread().getName() + " quiere entrar a la seccion critica");
        cerrojo.lock();
        System.out.println(Thread.currentThread().getName() + " hace la seccion critica");
        try {
            Thread.sleep(1000);
            System.out.println(Thread.currentThread().getName() + " sale de la seccion critica");
        } catch (InterruptedException ex) {
            System.out.println("Sleep roto");
        } finally {
            cerrojo.unlock();
        }
    }
}
