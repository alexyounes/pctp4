package TP4.E10;

/**
 *
 * @author Alejandro Younes
 */
public class Hilo implements Runnable {

    Compartido compartido;

    public Hilo(Compartido compartido) {
        this.compartido = compartido;
    }

    @Override
    public void run() {
        compartido.seccionCritica();
    }
}
