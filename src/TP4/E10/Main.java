package TP4.E10;

/**
 *
 * @author Alejandro Younes
 */
public class Main {

    public static void main(String[] args) {
        Compartido compartido = new Compartido();
        Hilo hilo = new Hilo(compartido);
        Thread hilo1 = new Thread(hilo, "Hilo 1");
        Thread hilo2 = new Thread(hilo, "Hilo 2");
        Thread hilo3 = new Thread(hilo, "Hilo 3");
        hilo1.start();
        hilo2.start();
        hilo3.start();
    }
}
