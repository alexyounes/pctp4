package Utilidades;

import Utilidades.Color.Colores;

/**
 *
 * @author Alejandro Younes
 */
public class PruebaColor {

    public static void main(String[] args) {
        Colores[] colores = Colores.values();
        for (Colores color : colores) {
            Color.escribirLineaColor("CADENA DE TEXTO", color);
        }
    }
}
