package Teoria;

/**
 *
 * @author Alejandro Younes
 */
public class Barbero implements Runnable {

    Barberia barberia;

    public Barbero(Barberia barberia) {
        this.barberia = barberia;
    }

    @Override
    public void run() {
        System.out.println("Barbero espera que llegue un cliente");
        barberia.barberoEsperando();
        System.out.println("Barbero atiende al cliente");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            System.out.println("Excepcion en el sleep del barbero");
        }
        System.out.println("Barbero avisa al cliente que termino");
        barberia.avisarCliente();
        System.out.println("Barbero espera que llegue otro cliente");
        barberia.barberoEsperando();
    }
}
