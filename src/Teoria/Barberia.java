package Teoria;

import java.util.concurrent.Semaphore;

/**
 *
 * @author Alejandro Younes
 */
public class Barberia {

    Semaphore semSillon = new Semaphore(1);
    Semaphore semBarbero = new Semaphore(0);
    Semaphore semCliente = new Semaphore(0);

    public boolean entrarBarberia() { // El cliente trata de sentarse
        return semSillon.tryAcquire();
    }

    public void salirBarberia() { // El cliente libera el sillon
        semSillon.release();
    }

    public void barberoEsperando() { // El barbero espera un cliente
        try {
            semBarbero.acquire();
        } catch (InterruptedException ex) {
            System.out.println("Excepcion al hacer que el barbero espere");
        }
    }

    public void avisarBarbero() { // El cliente avisa al barbero que llego
        semBarbero.release();
    }

    public void clienteEsperando() { // El cliente espera a que el barbero termine
        try {
            semCliente.acquire();
        } catch (InterruptedException ex) {
            System.out.println("Excepcion al hacer que el cliente espere");
        }
    }

    public void avisarCliente() { // El barbero avisa al cliente que termino
        semCliente.release();
    }
}
