package Teoria;

import java.util.concurrent.Semaphore;

/**
 *
 * @author Alejandro Younes
 */
public class BarberiaConSillas extends Barberia {

    Semaphore semSillon = new Semaphore(1);
    Semaphore semBarbero = new Semaphore(0);
    Semaphore semCliente = new Semaphore(0);
    Semaphore semMutexSillas = new Semaphore(1);
    int sillasLibres;

    public BarberiaConSillas(int sillasLibres) {
        this.sillasLibres = sillasLibres;
    }
    
    public boolean entrarBarberia() { // El cliente trata de sentarse
        boolean res = false;
        if (ocuparSilla()) {
            ocuparSillon();
            desocuparSilla();
            res = true;
        }
        return res;
    }

    private boolean ocuparSilla() { // El cliente intenta ocupar silla
        boolean res = false;
        System.out.println("Cliente intenta sentarse en una silla");
        try {
            semMutexSillas.acquire();
        } catch (InterruptedException ex) {
            System.err.println("Error al adquirir semMutexSillas");
        }
        if (sillasLibres > 0) {
            sillasLibres = sillasLibres - 1;
            res = true;
        }
        semMutexSillas.release();
        return res;
    }

    private void desocuparSilla() { // El cliente desocupa su silla
        try {
            semMutexSillas.acquire();
            System.out.println("Cliente desocupa su silla");
        } catch (InterruptedException ex) {
            System.err.println("Error al adquirir semMutexSillas");
        }
        sillasLibres = sillasLibres + 1;
        semMutexSillas.release();
    }

    private void ocuparSillon() { // El cliente ocupa el sillon
        try {
            semSillon.acquire();
            System.out.println("Cliente se sienta en el sillon");
        } catch (InterruptedException ex) {
            System.err.println("Error al adquirir semSillon");
        }
    }

    public void salirBarberia() { // El cliente libera el sillon
        System.out.println("Cliente se levanta del sillon y se va");
        semSillon.release();
    }

    public void barberoEsperando() { // El barbero espera un cliente
        try {
            semBarbero.acquire();
            System.out.println("Barbero queda esperando un cliente");
        } catch (InterruptedException ex) {
            System.out.println("Excepcion al hacer que el barbero espere");
        }
    }

    public void avisarBarbero() { // El cliente avisa al barbero que llego
        System.out.println("Cliente avisa al barbero que esta sentado");
        semBarbero.release();
    }

    public void clienteEsperando() { // El cliente espera a que el barbero termine
        try {
            semCliente.acquire();
            System.out.println("Cliente espera que el barbero termine");
        } catch (InterruptedException ex) {
            System.out.println("Excepcion al hacer que el cliente espere");
        }
    }

    public void avisarCliente() { // El barbero avisa al cliente que termino
        System.out.println("Barbero avisa al cliente que termino");
        semCliente.release();
    }
}
