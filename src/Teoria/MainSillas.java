package Teoria;

/**
 *
 * @author Alejandro Younes
 */
public class MainSillas {

    public static void main(String[] args) {
        BarberiaConSillas barberia = new BarberiaConSillas(5);
        Cliente cliente = new Cliente(barberia);
        Barbero barbero = new Barbero(barberia);
        Thread hiloBarbero = new Thread(barbero);
        Thread hiloCliente = new Thread(cliente);
        hiloBarbero.start();
        hiloCliente.start();
    }
}
