package Teoria;

/**
 *
 * @author Alejandro Younes
 */
public class Main {

    public static void main(String[] args) {
        Barberia barberia = new Barberia();
        Cliente cliente = new Cliente(barberia);
        Barbero barbero = new Barbero(barberia);
        Thread hiloBarbero = new Thread(barbero);
        Thread hiloCliente = new Thread(cliente);
        hiloBarbero.start();
        hiloCliente.start();
    }
}
