package Teoria;

/**
 *
 * @author Alejandro Younes
 */
public class Cliente implements Runnable {

    Barberia barberia;

    public Cliente(Barberia barberia) {
        this.barberia = barberia;
    }

    @Override
    public void run() {
        System.out.println("Cliente intenta entrar a la barberia");
        if (barberia.entrarBarberia()) {// Si entra en la barberia
            barberia.avisarBarbero();   // Avisa al barbero que se sentó en el sillón
            barberia.clienteEsperando();// Espera a que el barbero termine
            barberia.salirBarberia();   // Se va de la barberia
        } else {
            System.out.println("Cliente no pudo entrar a la barberia, se va");
        }
    }
}
